<?php
require 'Banks/IBank.php';
require 'Banks/Bank.php';
/**
 * CacheMoney
 *
 * Copyright (c) 2011 randy sesser <sesser@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author randy sesser <sesser@gmail.com>
 * @copyright 2011, randy sesser
 * @license http://www.opensource.org/licenses/mit-license The MIT License
 * @package CacheMoney
 * @filesource
 * @method mixed Get($key) Get a cache object by key
 * @method void Set($key, $value, $ttl = null) Cache an object using a key and optionally a ttl
 * @method boolean Check($key) Checks if a cached object exists in the 'Bank'
 * @method void Delete($key) Deletes an item from the cache
 * @method void Clear() Clears the current Bank account
 */
class CacheMoney
{
    /**
     * Configuration array for our cache
     * @var array 
     */
    protected static $configuration = array(
        'bank'		=> 'file',
        'account'	=> 'app',
        'prefix'	=> 'default',
        'ttl'		=> '1 hour',
        'path'		=> 'tmp/cache',
        'servers'	=> array(
            array(
                'hostname'	=> '127.0.0.1',
                'port'		=> 11211,
                'weight'	=> 100
            )
        ),
        'enable.gzip'	=> true
    );
    /**
     * An instance of an IBank
     * @var IBank 
     */
    protected static $instance = NULL;
    
    /**
     * An array of allowable static method calls. These allow
     * CacheMoney to be used like so: CacheMoney::get('some-key')
     * @var array 
     */
    private static $allowedMethods = array('get', 'set', 'check', 'delete', 'clear');
    
    /**
     * This is a static Class with no isntance variables
     */
    private final function __construct()
    {
        
    }
    
    /**
     * Configures and instantiates an IBank cache implementation
     * @param array $config Settings for your cache
     */
    public static function Configure(array $config = array())
    {
        static::$configuration = $config + static::$configuration;
        $classFile = ucfirst(static::$configuration['bank']) . '.php';
        include_once 'Banks' . DIRECTORY_SEPARATOR . $classFile;
        if (static::$instance == NULL) {
            $class = 'CacheMoney_Banks_' . ucfirst(static::$configuration['bank']);
            static::$instance = call_user_func_array(array($class, 'Instance'), array(static::$configuration));
        }
        
        if (isset(static::$configuration['path']) && !empty(static::$configuration['path'])) {
            if (!is_dir(static::$configuration['path'])) {
                @mkdir(static::$configuration['path'], 0755, true);
            }
        }
    }
    
    /**
     * 
     * @param string $name The name of the method to call
     * @param array $arguments And array of arguments to pass to the method
     * @return mixed Returns the result of the method call
     * @throws Exception
     */
    public static function __callStatic($name, $arguments)
    {
        if (in_array(strtolower($name), static::$allowedMethods)) {
            return call_user_func_array(array(static::$instance, strtolower($name)), $arguments);
        }
        throw new Exception("Call to undefined or private method in CacheManager");
    }
    
    
}
