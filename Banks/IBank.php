<?php
/**
 * CacheMoney
 *
 * Copyright (c) 2011 randy sesser <sesser@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author randy sesser <sesser@gmail.com>
 * @copyright 2011, randy sesser
 * @license http://www.opensource.org/licenses/mit-license The MIT License
 * @package CacheMoney
 * @filesource
 */

interface CacheMoney_Banks_IBank
{
	/**
	 * Get cache out of the bank
	 * @abstract
	 * @param string $key
	 * @return object
	 */
	public function get($key);

	/**
	 * Set (deposit) cache in the bank
	 * @abstract
	 * @param string $key
	 * @param object $data
	 * @param mixed $ttl
	 */
	public function set($key, $data, $ttl);

	/**
	 * Check to see if you have cache in the bank
	 * @abstract
	 * @param string $key
	 * @return boolean
	 */
	public function check($key);

	/**
	 * Delete cache
	 * @abstract
	 * @param string $key
	 */
	public function delete($key);

	/**
	 * Clear your account
	 * @abstract
	 */
	public function clear();
}
