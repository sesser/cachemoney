<?php

/**
 * CacheMoney
 *
 * Copyright (c) 2011 randy sesser <sesser@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author randy sesser <sesser@gmail.com>
 * @copyright 2011, randy sesser
 * @license http://www.opensource.org/licenses/mit-license The MIT License
 * @package CacheMoney
 * @filesource
 */

class CacheMoney_Banks_Memcache extends CacheMoney_Banks_Bank implements CacheMoney_Banks_IBank
{
	/**
	 *
	 * @var CacheMoney_Bank_Memcache
	 * @access private
	 * @static
	 */
	static $_instance = null;

	/**
	 *
	 * @var Memcache
	 * @access private
	 */
	private $mcache = null;

	private final function __construct(Array $config = array())
	{
		$this->setConfig($config);

		//-- Fallback on Memcache if Memcached is not available
		if (!class_exists("Memcache"))
			throw new Exception ("Memcache does not appear to be enabled on this server.");
			
		$this->mcache = new Memcache;
		$this->mcache->setCompressThreshold(10000, 0.25);
		$this->addServers();
	}

	public function get($key)
	{
		return $this->mcache->get($this->key($key));
	}

	public function set($key, $data, $ttl)
	{
		$compress = (is_bool($data) || is_int($data) || is_float($data)) ? false:MEMCACHE_COMPRESSED;
		if ($this->check($key)) {
			$this->mcache->replace($this->key($key), $data, $compress, $this->ttl($ttl)-time());
		} else {
			$this->mcache->add($this->key($key), $data, $compress, $this->ttl($ttl)-time());
		}
		
	}

	public function check($key)
	{
		return $this->mcache->get($this->key($key)) !== FALSE;
	}

	public function delete($key)
	{
		return $this->mcache->delete($this->key($key));
	}

	public function clear()
	{
		$this->mcache->flush();
		sleep(1);
	}


	/**
	 *
	 * @param array $config
	 * @return Memcach_Bank
	 */
	public static function Instance(Array $config = array())
	{
		$checksum = sha1(self::serialize($config));
		if (static::$_instance == null || $checksum != sha1(self::serialize(static::$_instance->$config)))
			static::$_instance = new self();
		static::$_instance->setConfig($config);
		return static::$_instance;
	}

	private function addServers()
	{
		$m =& $this->mcache;
		array_walk($this->config['servers'], function(&$server, $idx) use($m) {
			$m->addServer($server['host'], $server['port'], true, $server['weight']);
		});
	}
}

