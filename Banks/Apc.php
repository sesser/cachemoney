<?php
/**
 * CacheMoney
 *
 * Copyright (c) 2011 randy sesser <sesser@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author randy sesser <sesser@gmail.com>
 * @copyright 2011, randy sesser
 * @license http://www.opensource.org/licenses/mit-license The MIT License
 * @package CacheMoney
 * @filesource
 */

/**
 * An APC Bank
 */
class CacheMoney_Banks_Apc extends CacheMoney_Banks_Bank implements CacheMoney_Banks_IBank
{
	/**
	 *
	 * @var CacheMoney_Banks_Apc
	 * @access private
	 * @static
	 */
	static $_instance = null;
	private final function __construct(Array $config = array())
	{
		$this->setConfig($config);
		if (!function_exists('apc_cache_info'))
			throw new Exception("APC not available for banking");
	}

	public static function Instance(Array $config = array())
	{
		if (null == static::$_instance || sha1(self::serialize($config) !== sha1(self::serialize(static::$_instance->config))))
			static::$_instance = new self();
		static::$_instance->setConfig($config);
		return static::$_instance;
	}

	public function get($key)
	{
		$data = apc_fetch($this->key($key));
		if (false !== $data)
			return $data;
		return false;
	}

	public function set($key, $data, $ttl = null)
	{
		apc_store($this->key($key), $data, $this->ttl($ttl)-time());
	}

	public function check($key)
	{
		return apc_fetch($this->key($key)) !== false;
	}

	public function delete($key)
	{
		return apc_delete($this->key($key));
	}

	public function clear()
	{
		$it = new APCIterator('user', '/^'.$this->account().'/');
		while ($it->valid()) {
			$item = $it->current();
			apc_delete($item['key']);
			$it->next();
		}

	}
}
