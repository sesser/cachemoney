<?php
/**
 * CacheMoney
 *
 * Copyright (c) 2011 randy sesser <sesser@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author randy sesser <sesser@gmail.com>
 * @copyright 2011, randy sesser
 * @license http://www.opensource.org/licenses/mit-license The MIT License
 * @package CacheMoney
 * @filesource
 */
class CacheMoney_Banks_File extends CacheMoney_Banks_Bank implements CacheMoney_Banks_IBank
{
	private static $_instance = null;

	private $path = null;

	private final function __construct(Array $config = array())
	{
		$this->setConfig($config);
		$this->path = $this->config['path'] . DIRECTORY_SEPARATOR . $this->config['account'] . DIRECTORY_SEPARATOR;
		if (!is_dir($this->path))
			if (!mkdir($this->path, 0775, true))
				$this->enabled = false;

		$this->gc();
	}

	public static function Instance(Array $config = array())
	{
		if (null == static::$_instance || sha1(self::serialize($config) !== sha1(self::serialize(static::$_instance->config))))
			static::$_instance = new self($config);

		return static::$_instance;
	}

	protected function key($key)
	{
		return $this->prefix() . '_' . $key;
	}

	public function get($key)
	{
		$file = $this->path . $this->key($key) . '.cache';
		if (!$this->check($key))
			return false;

		if ($this->check($key)) {
			$ttl = file_get_contents($file . '.ttl');
			if (time() >= intval($ttl)) {
				$this->delete($key);
				return false;
			}
			return self::unserialize(file_get_contents($file), $this->config['enable.gzip']);
		}

		return false;

	}

	public function set($key, $data, $ttl = null)
	{

		$file = $this->path . $this->key($key) . '.cache';
		$ttl_file = $this->path . $this->key($key) . '.cache.ttl';
		file_put_contents($file, self::serialize($data, $this->config['enable.gzip']), LOCK_EX);
		file_put_contents($ttl_file, $this->ttl($ttl), LOCK_EX);
	}

	public function check($key)
	{
		$valid = true;
		if (!file_exists($this->path . $this->key($key) . '.cache'))
			$valid = false;

		if (!file_exists($this->path . $this->key($key) . '.cache.ttl'))
			$valid = false;

		if ($valid) {
			$ttl = file_get_contents($this->path . $this->key($key) . '.cache.ttl');
			$valid = time() <= intval($ttl);
		}

		if (!$valid)
			$this->delete($key);

		return $valid;
	}

	public function delete($key)
	{
		if (file_exists($this->path . $this->key($key) . '.cache'))
			unlink($this->path . $this->key($key) . '.cache');
		if (file_exists($this->path . $this->key($key) . '.cache.ttl'))
			unlink($this->path . $this->key($key) . '.cache.ttl');
	}

	public function clear()
	{
		$dir = new DirectoryIterator($this->path);
		while ($dir->valid())
			if (!$dir->isDot())
				unlink($dir->current());
	}

	protected function gc()
	{
		$dir = new DirectoryIterator($this->path);
		while ($dir->valid()) {
			if (!$dir->isDot() && preg_match('/.+\.cache$/i', $dir->getFilename())) {
				$key = str_replace($this->prefix().'_', '', $dir->getFilename());
				$key = str_replace('.cache', '', $key);
				if (!$this->check($key))
					$this->delete($key);
			}
			$dir->next();
		}
	}
}

