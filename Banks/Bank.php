<?php
/**
 * CacheMoney
 *
 * Copyright (c) 2011 randy sesser <sesser@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author randy sesser <sesser@gmail.com>
 * @copyright 2011, randy sesser
 * @license http://www.opensource.org/licenses/mit-license The MIT License
 * @package CacheMoney
 * @filesource
 */

/**
 * Bank - an abstract implementaion of an IBank. All Banks must inherit
 * from this class and implement an IBank
 *
 * @abstract
 */
abstract class CacheMoney_Banks_Bank implements CacheMoney_Banks_IBank
{
	/**
	 * Enables/Disables the bank
	 * @var boolean
	 * @access protected
	 */
	protected $enabled = true;

	/**
	 * Bank configuration as passed in from CacheMoney_Manager::GetBank()
	 * @var array
	 * @access protected
	 */
	protected $config = null;

	/**
	 * Setter for the protected config object
	 * @param array $config Configuration to set
	 */
	protected function setConfig(Array $config = array())
	{
		$this->config = $config;
	}

	/**
	 * Returns a key which identifies an object in the bank. Concatinates
	 * the account, prefix and key to create the idea of gourping objects
	 * @param string $key The key identifying an object in the bank
	 * @return string
	 */
	protected function key($key)
	{
		return $this->account() . '__' . $this->prefix() . '_' . $key;
	}

	/**
	 * Gets the configured account or a sensible default of 'savings'
	 * @return string
	 */
	protected function account()
	{
		return (isset($this->config['account']) && !empty($this->config['account'])) ? $this->config['account']:'savings';
	}

	/**
	 * Gets the configured prefix or a sensible default.. like 'default'
	 * @return string
	 */
	protected function prefix()
	{
		return (isset($this->config['prefix']) && !empty($this->config['prefix'])) ? $this->config['prefix']:'default';
	}

	/**
	 * Parses the input value into an integer value from an English textual datetime description
	 * @param string $ttl A string representing the lifetime of an object in the bank (@link http://us.php.net/strtotime strtotime)
	 * @return int
	 */
	protected function ttl($ttl = null)
	{
		if (!empty($ttl)) {
			$ttl = strtotime($ttl);
			if ($ttl !== -1)
				return $ttl;
		} else {
			$ttl = strtotime($this->config['ttl']);
			if ($ttl !== -1)
				return $ttl;
		}

		return $ttl;
	}

	/**
	 * Serializes the data in preparation for deposit. Optionally compresses the data with gzip
	 * @static
	 * @param mixed $data Some object to serialize
	 * @param bool $compress True to compress the serialized data with gzip
	 * @return string
	 */
	protected static function serialize($data, $compress = false)
	{
		if ($compress)
			return gzcompress(serialize($data));
		return serialize($data);
	}

	/**
	 * Unserializes the data returned from a Bank. Optionally decompresses the data with gzip
	 * @static
	 * @param string $data Some serialized data to unserialize
	 * @param bool $decompress True to decompress the serialized data with gzip
	 * @return mixed
	 */
	protected static function unserialize($data, $decompress = false)
	{
		if ($decompress)
			return unserialize(gzuncompress($data));

		return unserialize($data);
	}
}
