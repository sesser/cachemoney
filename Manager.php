<?php
require 'Banks/IBank.php';
require 'Banks/Bank.php';
/**
 * CacheMoney
 *
 * Copyright (c) 2011 randy sesser <sesser@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author randy sesser <sesser@gmail.com>
 * @copyright 2011, randy sesser
 * @license http://www.opensource.org/licenses/mit-license The MIT License
 * @package Money
 * @filesource
 */
class CacheMoney_Manager
{
	/**
	 * Default configuration
	 * @staticvar array $defaults The default configuration for this MoneyManager
	 * @access protected
	 */
	protected static $defaults = array(
		'bank'		=> 'file',
		'account'	=> 'app',
		'prefix'	=> 'default',
		'ttl'		=> '1 hour',
		'path'		=> '/tmp/cache/',
		'servers'	=> array(
			array(
				'hostname'	=> '127.0.0.1',
				'port'		=> 11211,
				'weight'	=> 100
			)
		),
		'enable.gzip'	=> true
	);

	/**
	 * Method to get an instance of an IBank where you can deposit/withdrawl cache
	 * @access public
	 * @static
	 * @param array $config Array of settings for this 'bank'
	 * @return Banks\IBank Returns an IBank in which to store your cache
	 */
	public static function GetBank(Array $config = array())
	{
		$config = array_merge(static::$defaults, $config);
		$fileName = ucwords($config['bank']) . '.php';
		$className = 'CacheMoney_Banks_' . ucwords($config['bank']);
		require_once 'CacheMoney/Banks/' . $fileName;
		$bank = null;
		if (class_exists($className)) {
			try {
				$meth = new ReflectionMethod($className, 'Instance');
				$bank = $meth->invokeArgs(null, array('config' => $config));
			} catch (ReflectionException $rex) {
				throw new Exception("Could not find your bank, " . $className . " :: " . $rex->getMessage());
			}
		} else {
			throw new Exception("Could not find your bank, " . $className);
		}
		return $bank;
	}
}
