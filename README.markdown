## CacheMoney

CacheMoney is a simple cache library written in PHP. It supports a couple of cache stores
out of the box (APC and File based) with a 3rd (Memcache) planned. It should be easy
to implement any kind of cache store you want (Database, NoSQL, eAccelerator, xcache, etc).

## Crash Course

	require 'CacheMoney/CacheMoney.php';
	
	$config = array(
		'bank' => 'File',
		'account' => 'myAccount',
		'ttl' => '5 minutes',
		'path' => 'tmp/cache/'
	);
	
	CacheMoney::Configure($config);
	
	if (false === ($data = CacheMoney::Get('somekey'))) {
		$data = array(
			'id' => 1,
			'title' => 'Test',
			'content' => 'This is content'
		);
		CacheMoney::Set('somekey', $data, '1 minute');
	}
	
	


 
